from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import home
from django.test import LiveServerTestCase
from selenium import webdriver
import unittest
from selenium.webdriver.chrome.options import Options

# # Create your tests here.

class Story7(TestCase):

    def test_url(self):
        response = Client().get("")
        self.assertEqual(response.status_code,200)
    
    def test_view(self):
        response = Client().get("")
        self.assertTemplateUsed(response,'main/home.html')

class FuncTest(TestCase):
    
    def tearDown(self):
        self.selenium.quit()