from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import books,data
from django.test import LiveServerTestCase
from .apps import Story8Config
# # Create your tests here.

class Story8(TestCase):

    def test_url(self):
        response = Client().get("/story8/")
        self.assertEqual(response.status_code,200)
    
    def test_view(self):
        response = Client().get("/story8/")
        self.assertTemplateUsed(response,'books.html')

    def test_json_url(self):
        response = Client().get('/story8/data/')
        self.assertEqual(response.status_code,200)


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story8Config.name, 'story8')