from django.urls import path
from .views import books,data


appname = 'Story8'

urlpatterns = [
   path('', books, name = 'books_list'),
   path('data/', data, name='books_data'),
]
