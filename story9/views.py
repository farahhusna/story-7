from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect

# Create your views here.
def index(request):
	return render(request, 'index.html')

def sign_up(request):
	if request.method == 'POST':
	    username = request.POST['username']
	    email = request.POST['email']
	    password = request.POST['password']
	    if User.objects.filter(username=username).exists():
	    	messages.error(request, 'Username is not available!')
	    else:
	    	user = User.objects.create_user(username, email, password)
	    	return redirect('/story9/login')

	return render(request, 'signup.html')

def log_in(request):
	if request.method == 'POST':
	    username = request.POST['username']
	    password = request.POST['password']
	    if User.objects.filter(username=username).exists():
		    user = authenticate(request, username=username, password=password)
		    if user is not None:
		        login(request, user)
		        return redirect('/story9/')
		    else:
		    	messages.error(request, 'Wrong password!')
	    else:
	    	messages.error(request, 'Wrong username!')
	
	return render(request, 'login.html')

def log_out(request):
	logout(request)
	return redirect('/story9/')


def profile(request):
	if request.method == 'POST':
		user = User.objects.get(pk=request.POST['id'])
		user.email = request.POST['email']
		user.profile.bio = request.POST['bio']
		user.save()
		return redirect('/story9/')

	return render(request, 'update.html')
