from .apps import Story9Config
from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
from django.contrib.auth.models import User


# Create your tests here.
class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story9Config.name, 'story9')
		self.assertEqual(apps.get_app_config('story9').name, 'story9')

class Story9(TestCase):
	def test_homepage_url_exists(self):
		response = Client().get('/story9/')
		self.assertEqual(response.status_code, 200)

	def test_signup_url_exists(self):
		response = Client().get('/story9/signup/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'signup.html')

	def test_login_url_exists(self):
		response = Client().get('/story9/login/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'login.html')

	def test_logout_url_exists(self):
		response = Client().get('/story9/logout/')
		self.assertEqual(response.status_code, 302)

	def test_update_profile_url_exists(self):
		response = Client().get('/story9/update/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'update.html')


	def test_create_model(self):
		User.objects.create_user(username="aku", email="aku@gmail.com", password="aku")
		self.assertEqual(User.objects.all().count(), 1)
